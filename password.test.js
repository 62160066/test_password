const { checkLength, checkAlphabet, checkSymbol, checkPassword, checkDigit } = require('./password')
describe('Test Password Length', () => {
  test('should 8 characters to be true', () => {
    expect(checkLength('12345678')).toBe(true)
  })
  test('should 7 characters to be false', () => {
    expect(checkLength('1234567')).toBe(false)
  })
  test('should 25 characters to be true', () => {
    expect(checkLength('1111111111111111111111111')).toBe(true)
  })
  test('should 26 characters to be false', () => {
    expect(checkLength('11111111111111111111111111')).toBe(false)
  })
})
describe('Test Alphabet', () => {
  test('should has alphabet m in password', () => {
    expect(checkAlphabet('m')).toBe(true)
  })
  test('should has not alphabet in password', () => {
    expect(checkAlphabet('1111')).toBe(false)
  })
  test('should has alphabet A in password', () => {
    expect(checkAlphabet('A')).toBe(true)
  })
  test('should has alphabet Z in password', () => {
    expect(checkAlphabet('m')).toBe(true)
  })
})
describe('test symbol', () => {
  test('should has Symbol ! in password to be true', () => {
    expect(checkSymbol('11!11')).toBe(true)
  })
  test('should has not Symbol  in password to be false', () => {
    expect(checkSymbol('11211')).toBe(false)
  })
})
describe('test password', () => {
  test('should has zone@11 to be false ', () => {
    expect(checkPassword('zone@11')).toBe(false)
  })
  test('should has zone@111 to be true ', () => {
    expect(checkPassword('zone@111')).toBe(true)
  })
  test('should has Mankhong@@@@@@123456789000 to be false ', () => {
    expect(checkPassword('Mankhong@@@@@@123456789000')).toBe(false)
  })
  test('should has 0995697894!zone to be true ', () => {
    expect(checkPassword('0995697894!zone')).toBe(true)
  })
  test('should has 12345 to be false ', () => {
    expect(checkPassword('12345')).toBe(false)
  })
  test('should has zone12345 to be false ', () => {
    expect(checkPassword('zone12345')).toBe(false)
  })
})
describe('test digit', () => {
  test('should has digit in password to be true', () => {
    expect(checkDigit('2222')).toBe(true)
  })
  test('should has not digit in password to be false', () => {
    expect(checkDigit('aaaaa')).toBe(false)
  })
})
